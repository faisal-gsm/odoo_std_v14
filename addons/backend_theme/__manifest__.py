# -*- coding: utf-8 -*-
{
    'name': 'Backend theme',
    'category': 'Hidden',
    'version': '1.0',
    'description':
        """
        """,
    'depends': ['web','mail'],
    'auto_install': True,
    'data': [
        'views/webclient_templates.xml',
    ],
    'qweb': [
        
        'static/src/xml/*.xml',
        'static/src/bugfix/bugfix.xml',
    ],
}
